package cn.bdqn.invitation;

import org.junit.jupiter.api.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisTest {

    @Test
    public void test1(){
        Jedis jedis = new Jedis("192.168.3.206", 6379);
        jedis.set("jedisTest1","jedisTest001");
        jedis.set("jedisTest2","jedisTest002");
        jedis.close();
    }
    @Test
    public void test2(){
        Jedis jedis = getJedisPool().getResource();
        jedis.hset("person","name","zhangsan");
        jedis.hset("person","age","20");
        jedis.close();
    }

    public JedisPool getJedisPool(){

        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(50);
        jedisPoolConfig.setMaxWaitMillis(20);

        JedisPool jedisPool = new JedisPool(jedisPoolConfig, "192.168.3.206", 6379);
        return jedisPool;
    }





}
