$(function(){
    // Vue 对象 设置
    var app = new Vue({
        el: "#app",
        data:{
            list:'',
            pageNum:'',
            pages:'',
            prePage:'',
            nextPage:'',
            title:''
        },
        methods:{
            changePage:function(pageIndex,title){
                var _this = this;
                console.log("开始第二页查询-------------");
                var url = "/invitation/list";
                var params={
                    offset:pageIndex,
                    limit:4,
                    title:title
                };
                $.getJSON(url,params,function(data){
                    console.log("$.getJSON"+this);
                    _this.list=data.list;
                    _this.pageNum=data.pageNum;
                    _this.pages=data.pages;
                    _this.prePage=data.prePage < 1 ? 1 : data.prePage;
                    // debugger
                    _this.nextPage=data.nextPage == 0 ? data.pages : data.nextPage;
                    _this.title=data.title;
                });
            },

            fnnextPage:function(){
                this.changePage(this.nextPage,this.title);
            },
            fnprePage:function(){
                this.changePage(this.prePage,this.title);
            },
            fnsearch:function(){
                this.changePage(this.prePage,this.title);
            }
        },
        created:function(){
            this.changePage();
        }
    });

});
