/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.19-log : Database - invitation
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`invitation` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `invitation`;

/*Table structure for table `invitation` */

DROP TABLE IF EXISTS `invitation`;

CREATE TABLE `invitation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '帖子编号',
  `title` varchar(100) NOT NULL COMMENT '帖子标题',
  `summary` varchar(100) DEFAULT NULL COMMENT '内容摘要',
  `author` varchar(100) DEFAULT NULL COMMENT '作者',
  `createdate` date NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='帖子表';

/*Data for the table `invitation` */

insert  into `invitation`(`id`,`title`,`summary`,`author`,`createdate`) values (1,'最新宝马谍照','新款宝马跑车','小和','2019-11-08'),(2,'恒大的三杆洋枪','外援给力','小和','2019-10-12'),(3,'北京马拉松','北马开跑','小和','2019-08-08'),(4,'庆余年热播','从前有座灵剑山','小和','2019-12-05'),(5,'歼31试飞','国产歼31登辽宁舰','小和','2019-10-10');

/*Table structure for table `reply_detail` */

DROP TABLE IF EXISTS `reply_detail`;

CREATE TABLE `reply_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `invid` bigint(20) NOT NULL COMMENT '帖子编号',
  `content` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '回复内容',
  `author` varchar(100) COLLATE utf8_unicode_ci DEFAULT '匿名用户' COMMENT '回复人昵称',
  `createdate` date DEFAULT NULL COMMENT '恢复时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='帖子回复表';

/*Data for the table `reply_detail` */

insert  into `reply_detail`(`id`,`invid`,`content`,`author`,`createdate`) values (7,1,'什么时候上市 售价多少','总算摇上号了','2019-11-10'),(8,1,'16W发动机,百米加速2.6秒','我是MINI','2019-11-10'),(9,1,'这个车扭矩是多少','龙行天下','2019-11-10');

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `name` bigint(10) NOT NULL AUTO_INCREMENT,
  `age` bigint(10) DEFAULT NULL COMMENT '年龄',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*Data for the table `student` */

insert  into `student`(`name`,`age`) values (10,20),(20,30);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
