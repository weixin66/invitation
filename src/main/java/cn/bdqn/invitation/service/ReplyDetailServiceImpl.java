package cn.bdqn.invitation.service;

import cn.bdqn.invitation.mapper.ReplyDetailMapper;
import cn.bdqn.invitation.pojo.ReplyDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ReplyDetailServiceImpl implements  ReplyDetailService{
    @Autowired
    private ReplyDetailMapper replyDetailMapper;

    @Override
    public List<ReplyDetail> selectByInvid(Long id) {
        return replyDetailMapper.selectByInvid(id);
    }

    @Override
    public int insert(ReplyDetail record) {

        return replyDetailMapper.insert(record);
    }

}
