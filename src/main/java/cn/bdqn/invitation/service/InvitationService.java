package cn.bdqn.invitation.service;

import cn.bdqn.invitation.pojo.Invitation;
import com.github.pagehelper.PageInfo;

public interface InvitationService {
    // 分页+条件查询方法
    PageInfo<Invitation> selectByTitle(Integer offset , Integer limit,String title);
}
