package cn.bdqn.invitation.service;

import cn.bdqn.invitation.mapper.InvitationMapper;
import cn.bdqn.invitation.pojo.Invitation;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvitationServiceImpl implements InvitationService{

    @Autowired
    private InvitationMapper invitationMapper;

    @Override
// 缓存结果 key: "selectByTitle::SimpleKey [0,4,]"
@Cacheable(cacheNames = "selectByTitle")
public PageInfo<Invitation> selectByTitle(Integer offset, Integer limit, String title) {
    // 开始分页
    PageHelper.startPage(offset,limit);
    List<Invitation> invitations = invitationMapper.selectByTitle(title);
    // 编写 分页详情
    PageInfo<Invitation> pageInfo = new PageInfo<>(invitations);
    return pageInfo;
}
}
