package cn.bdqn.invitation.service;

import cn.bdqn.invitation.pojo.ReplyDetail;

import java.util.List;

public interface ReplyDetailService {
    // 根据 帖子编号 查询对应的回复列表
    List<ReplyDetail> selectByInvid(Long id);

    int insert(ReplyDetail record);
}
