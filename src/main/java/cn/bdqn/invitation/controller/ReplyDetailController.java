package cn.bdqn.invitation.controller;

import cn.bdqn.invitation.pojo.ReplyDetail;
import cn.bdqn.invitation.service.ReplyDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

// 回帖列表
@Controller
@RequestMapping("replydetail")
public class ReplyDetailController {

    @Autowired
    private ReplyDetailService replyDetailService;

    // http://localhost:8080/replydetail/selectByInvid?invid=1
    // 根据 帖子编号 查询对应的回复列表
    @RequestMapping("selectByInvid")
    public String selectByInvid(@RequestParam("invid") Long invid, Model model){
        List<ReplyDetail> replyDetails = replyDetailService.selectByInvid(invid);
        model.addAttribute("list",replyDetails);
        return "replydetail";
    }

    // 视图跳转
    @RequestMapping("addview/{invid}")
    public String addview(@PathVariable("invid") Long invid , Model model){
        model.addAttribute("invid",invid);
        return "replyadd";
    }
    // 接受 回复信息的功能
    @RequestMapping("replyadd")
    public String replyadd(ReplyDetail replyDetail,Model model){
        replyDetailService.insert(replyDetail);
        // selectByInvid(replyDetail.getInvid(),model);
        return "forward:selectByInvid?invid="+replyDetail.getInvid();
    }
}
