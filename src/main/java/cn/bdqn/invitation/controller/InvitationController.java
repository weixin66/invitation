package cn.bdqn.invitation.controller;

import cn.bdqn.invitation.pojo.Invitation;
import cn.bdqn.invitation.service.InvitationService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPObject;
import com.github.pagehelper.PageInfo;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

@Controller
@RequestMapping("invitation")
public class InvitationController {

    @Autowired
    private InvitationService invitationService;

    @RequestMapping("list")
    @ResponseBody  // JSON 格式
    public Object selectByTile(@RequestParam(value = "offset",required = false,defaultValue = "1") Integer offset,
                                 @RequestParam(value = "limit",required = false,defaultValue = "4") Integer limit,
                                 @RequestParam(value = "title",required = false) String title) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {


        PageInfo<Invitation> pageInfo = invitationService.selectByTitle(offset, limit, title);
        // Object obj = JSON.toJSON(pageInfo);
        // JSONObject jsonObject = JSONObject.parseObject(JSON.toJSONString(pageInfo));
        JSONObject jsonObject = (JSONObject) JSONObject.toJSON(pageInfo);
        jsonObject.put("title",title);
        return jsonObject;
    }

}
