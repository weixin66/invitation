package cn.bdqn.invitation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BaseController {

    @RequestMapping("/")
    public String base(){
        // 跳转首页
        return "invitationlist";
    }

    @RequestMapping("/vue")
    public String vue(){
        // 跳转首页
        return "vue";
    }
}
