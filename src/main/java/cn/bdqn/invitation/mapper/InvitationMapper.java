package cn.bdqn.invitation.mapper;

import cn.bdqn.invitation.pojo.Invitation;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InvitationMapper {

    /**
     * 根据 标题 搜索 所有帖子
     */
    List<Invitation> selectByTitle(@Param("title") String title);

    int deleteByPrimaryKey(Long id);

    int insert(Invitation record);

    int insertSelective(Invitation record);

    Invitation selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Invitation record);

    int updateByPrimaryKey(Invitation record);
}