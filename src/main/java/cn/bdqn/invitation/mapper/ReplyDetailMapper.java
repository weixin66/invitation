package cn.bdqn.invitation.mapper;

import cn.bdqn.invitation.pojo.ReplyDetail;

import java.util.List;

public interface ReplyDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ReplyDetail record);

    int insertSelective(ReplyDetail record);

    ReplyDetail selectByPrimaryKey(Long id);

    // 根据 帖子编号 查询对应的回复列表
    List<ReplyDetail> selectByInvid(Long id);

    int updateByPrimaryKeySelective(ReplyDetail record);

    int updateByPrimaryKey(ReplyDetail record);
}